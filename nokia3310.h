#ifndef NOKIA3310_H_INCLUDED
#define NOKIA3310_H_INCLUDED
#include "common.h"

#define USE_DOUBLE_SIZE_FONT

// Init
extern void nokia_init(void);

// Contrast
extern u08 nokia_get_contrast (void);
extern void nokia_set_contrast(u08 contrast);

// inversion
extern u08 nokia_get_inverse(void);
extern void nokia_set_inverse(u08 inv);

// data
extern void nokia_data(u08 ch);

// text
extern void nokia_cls(void);
extern void nokia_gotoxy(u08 x, u08 y);
extern void nokia_putchar(u08 ch);
extern void nokia_puts(const u08 *s);
extern void nokia_puts_bg(const u08 *s);
extern void nokia_puts_progmem(const char *s);

#ifdef USE_DOUBLE_SIZE_FONT
extern void nokia_putchar_doubled(u08 ch);
extern void nokia_puts_doubled(u08 * s);
extern void nokia_puts_doubled_bg(u08 * s);
#endif

#define nokia_puts_p(s)	nokia_puts_progmem(PSTR(s))


#endif  // NOKIA3310_H_INCLUDED

