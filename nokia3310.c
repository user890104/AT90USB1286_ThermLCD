#include <avr/pgmspace.h>
#include "nokia3310.h"
#include "timer0.h"


// ноги для управления ЖКИ.
#define  NOKIA_SCK		A, 0, H	//	(нога 2 на дисплее)
#define  NOKIA_DOUT		A, 1, H	//	(нога 3 на дисплее)
#define  NOKIA_D_C		A, 2, H	//	(нога 4 на дисплее)
#define  NOKIA_RESET	A, 3, H	//	(нога 8 на дисплее)

// Остальные ноги ЖКИ:
// 1 - VDD(3.3в)
// 5 - CS (замкнут на землю)
// 6 - GND
// 7 - VOUT (замкнут на землю через кондёр 1 мкф))

// Размеры экрана в точках
#define GLCD_PIXELX			(84)
#define GLCD_PIXELY			(48)

// Команды ЖКИ (normal Functionset)
#define GLCD_FUNCTIONSET	(0x20)
#define GLCD_SETXADDR		(0x80)
#define GLCD_SETYADDR		(0x40)
#define	GLCD_DISPLAYBLANK	(0x08)
#define	GLCD_DISPLAYFLUSH	(0x09)
#define	GLCD_DISPLAYNORMAL	(0x0C)
#define	GLCD_DISPLAYINVERT	(0x0D)

// Команды ЖКИ (extended Functionset)
#define GLCD_FUNCTIONSETEXT	(0x21)
#define GLCD_SET_BIAS		(0x10)
#define GLCD_SET_VOP		(0x80)
#define GLCD_SET_TEMPCOEF	(0x04)

#define GLCD_BIAS_1_100		(0x00)	// 1:100
#define GLCD_BIAS_1_80		(0x01)	// 1:80
#define GLCD_BIAS_1_65		(0x02)	// 1:65
#define GLCD_BIAS_1_48		(0x03)	// 1:48
#define GLCD_BIAS_1_40		(0x04)	// 1:40/1:34
#define GLCD_BIAS_1_24		(0x05)	// 1:24
#define GLCD_BIAS_1_18		(0x06)	// 1:18/1:16
#define GLCD_BIAS_1_10		(0x07)	// 1:10/1:9/1:8

#define GLCD_TEMPCOEF_0     (0x00)  // TF = 0
#define GLCD_TEMPCOEF_1     (0x01)  // TF = 1
#define GLCD_TEMPCOEF_2     (0x02)  // TF = 2
#define GLCD_TEMPCOEF_3     (0x03)  // TF = 3

// Шрифт
static u08 __attribute__ ((progmem)) Font5x7[] = {
#include "font_dos_ru.inc"
};

// вспомогательные переменные
static u08 nokia_contrast;
static u08 nokia_inverse;
static u08 nokia_x;
static u08 nokia_y;


// запись байта в ЖКИ (программный SPI)
void nokia_w(u08 ch)
{
	u08 i;	// счётчик

	for (i = 8; i; i--)	// передаём 8 бит, начиная со старшего
	{
		// выставляем ногу данных в соответствии со значением старшего разряда ch
		if (ch & 0x80)	on(NOKIA_DOUT);
		else			off(NOKIA_DOUT);
		// защёлкиваем бит
		off(NOKIA_SCK);
		// сдвигаем в старший разряд следующий бит
		ch <<= 1;
		// возвращаем клок на место
		on(NOKIA_SCK);
	}
}

// отправка команды в ЖКИ
static void nokia_cmd(u08 cmd)
{
	// выставляем признак команды - D/C = 0
	off(NOKIA_D_C);
	// пишем байт
	nokia_w(cmd);
}

// отправка байта данных в ЖКИ
void nokia_data(u08 ch)
{
	// выставляем признак данных - D/C = 1
	on(NOKIA_D_C);
	// пишем байт
	nokia_w(ch);
	// модифицируем позицию в экранном буфере.
	if (++nokia_x > GLCD_PIXELX-1)
	{
		nokia_x = 0;
		nokia_y++;
	}
}

// установка адреса в ЖКИ
static void nokia_set_addr(u08 x, u08 y)
{
	if (y > 5) y = 5;								// проверка максимального значения строки
	if (x > GLCD_PIXELX - 1) x = GLCD_PIXELX - 1;	// проверка максимального значения столбца
	// модифицируем позицию в экранном буфере.
	nokia_x = x;
	nokia_y = y;
	// команды для установки адреса
	nokia_cmd(GLCD_SETYADDR | y);
	nokia_cmd(GLCD_SETXADDR | x);
}

// переход к координатам x, y (в смысле вывода текста шрифтом 5х7)
void nokia_gotoxy(u08 x, u08 y)
{
	nokia_set_addr(((x<<1) + x) << 1, y);	//	nokia_set_addr(x * 6, y);
}

static inline u08 nokia_where_x(void)
{
	return nokia_x;
}

static inline u08 nokia_where_y(void)
{
	return nokia_y;
}

// вычисление начала символа в таблице шрифта
static u16 char_to_pos(u08 ch)
{
	u16 pos;
	// текст у меня в кодировке DOS (cp866), там дыра между "п" и "р". Потому:
	//if (ch > 'п') ch -= ('р' - 'п' - 1);
	// символы до 0x20 (пробел) тоже выкинуты
	ch -= 0x20;
	// находим позицию в таблице шрифта (5 байт на символ)
	pos = ch;
	pos += (pos << 2); //* 5;
	return pos;
}

// вывод символа на ЖКИ. Параметр - код символа.
void nokia_putchar(u08 ch)
{
	u16 pos = char_to_pos(ch);

	// выводим символ (5 последовательных байт)
	for (ch = 5; ch; ch--)
		nokia_data(pgm_read_byte(&Font5x7[pos++]));

	// выводим промежуток между символами
	nokia_data(0);
}

static void nokia_ddata(u08 b)
{
	nokia_data(b);
	nokia_data(b);
}

// очистка экрана
void nokia_cls(void)
{
	u08 i;

	// переход в начало экрана
	nokia_gotoxy(0, 0);

	// заполняем экран нулями
	for(i = 252; i; i--)
		nokia_ddata(0);

	// возврат в начало экрана
	nokia_gotoxy(0, 0);
}

// вывод строки (строка в ОЗУ)
void nokia_puts(const u08 *s)
{
    while (*s)
        nokia_putchar(*s++);
}

// вывод строки (строка в ОЗУ)
void nokia_puts_bg(const u08 *s)
{
    while (*s) {
        u08 c = *s++;
        nokia_putchar(c >= 0x40 ? c + 0x40 : c);
    }
}

// вывод строки (строка в ПЗУ)
void nokia_puts_progmem(const char *s)
{
	register u08 c;
	while((c = pgm_read_byte(s++)))
		nokia_putchar(c);
}

// вернуть текущее значение контраста
inline u08 nokia_get_contrast(void)
{
	return nokia_contrast;
}

// задать значение контраста (он же VOP)
void nokia_set_contrast(u08 contrast)
{
	nokia_contrast = contrast;
	nokia_cmd(GLCD_FUNCTIONSETEXT);
	nokia_cmd(GLCD_SET_VOP | contrast);
	nokia_cmd(GLCD_FUNCTIONSET);
}

// вернуть текущее значение инверсии
inline u08 nokia_get_inverse(void)
{
	return nokia_inverse;
}

// задать значение инверсии
void nokia_set_inverse(u08 inv)
{
	nokia_inverse = inv;
	if(inv)
		nokia_cmd(GLCD_DISPLAYINVERT);
	else
		nokia_cmd(GLCD_DISPLAYNORMAL);
}


#ifdef USE_DOUBLE_SIZE_FONT
static void put_nibble(u08 b)
{
	u08 w = 0;

	if (b & 1) w |= 0x03;
	if (b & 2) w |= 0x0C;
	if (b & 4) w |= 0x30;
	if (b & 8) w |= 0xC0;

	nokia_ddata(w);
}

// вывод символа на ЖКИ. Параметр - код символа.
void nokia_putchar_doubled(u08 ch)
{
	u08 b;
	u16 pos = char_to_pos(ch);

	for (ch = 5; ch; ch--)
	{
		b = pgm_read_byte(&Font5x7[pos++]);
		put_nibble(b);
	}
	nokia_ddata(0);

	pos-=5;
	nokia_set_addr(nokia_where_x()-12,nokia_where_y()+1);


	for (ch = 5; ch; ch--)
	{
		b = pgm_read_byte(&Font5x7[pos++]);
		b >>= 4;
		put_nibble(b);
	}
	// выводим промежуток между символами
	nokia_ddata(0);

	nokia_set_addr(nokia_where_x(),nokia_where_y()-1);
}

void nokia_puts_doubled(u08 * s)
{
    while (*s)
        nokia_putchar_doubled(*s++);
}

void nokia_puts_doubled_bg(u08 * s)
{
    while (*s) {
        u08 c = *s++;
        nokia_putchar_doubled(c >= 0x40 ? c + 0x40 : c);
    }
}

#endif

// инициализация ЖКИ
void nokia_init(void)
{
	// сначала инициализируем ножки
	on(NOKIA_SCK);
	on(NOKIA_RESET);
	direct(NOKIA_SCK, O);
	direct(NOKIA_RESET, O);
	direct(NOKIA_DOUT, O);
	direct(NOKIA_D_C, O);

	// Сброс ЖКИ
	off(NOKIA_RESET);
	asm volatile ("nop"); asm volatile ("nop");
	asm volatile ("nop"); asm volatile ("nop");
//	delay_ms(5);
	on(NOKIA_RESET);

	// Включаем расширенный набор команд
	nokia_cmd(GLCD_FUNCTIONSETEXT);
	// Задаём температурный коэффициент
	nokia_cmd(GLCD_SET_TEMPCOEF | GLCD_TEMPCOEF_2);
	// Задаём режим мультиплексирования 1:65
	nokia_cmd(GLCD_SET_BIAS | GLCD_BIAS_1_65);
	// Контраст (заодно отключает расширенный набор команд)
	nokia_set_contrast(65);
	// выключим инверсию.
	nokia_set_inverse(FALSE);

	// Очистим
//	nokia_cls();
}

