#include <avr/interrupt.h>

#include "timer0.h"

// частота возникновения таймерного прерывания
#define	TIMER0_FREQ			(1000)

// варианты прескалера
//#define	TIMER0_PRESCALER	(1)
//#define	TIMER0_PRESCALER	(8)
#define	TIMER0_PRESCALER	(64)
//#define	TIMER0_PRESCALER	(256)
//#define	TIMER0_PRESCALER	(1024)

#define	TIMER0_RELOAD_LO	((0xFFFF-(F_CPU / TIMER0_PRESCALER / TIMER0_FREQ)+1) & 0xFF)
#define	TIMER0_RELOAD_HI	((0xFFFF-(F_CPU / TIMER0_PRESCALER / TIMER0_FREQ)+1) >> 8)

// проверка, что таймер может работать с выбранной частотой
#if (TIMER0_RELOAD_HI!=0xFF)
#error TIMER0_RELOAD_HI!=0xFF!
#endif

static volatile u08 ms_ticks_byte;

// инициализация таймера
void timer0_init(void)
{
	// программируем прескалер
#if (TIMER0_PRESCALER==1)
	TCCR0B |= _BV(CS10);	// clock source = clkio / 1
#elif (TIMER0_PRESCALER==8)
	TCCR0B |= _BV(CS11);
#elif (TIMER0_PRESCALER==64)
	TCCR0B |= _BV(CS11) | _BV(CS10);
#elif (TIMER0_PRESCALER==256)
	TCCR0B |= _BV(CS12);
#elif (TIMER0_PRESCALER==1024)
	TCCR0B |= _BV(CS12) | _BV(CS10);
#else
	#error TIMER0_PRESCALER not set properly!
#endif
	// заносим значение для перезагрузки
	TCNT0 = TIMER0_RELOAD_LO;
	// разрешаем прерывание по переполнению таймера
	TIMSK0 |= _BV(TOIE0);
}

// задержка на заданное число милисекунд
void delay_ms(u08 ms)
{
	ms_ticks_byte = ms;
	while (ms_ticks_byte);
}

// обработчик прерывания по переполнению таймера
ISR(TIMER0_OVF_vect)
{
	// сбрасываем флаг прерывания
	TCNT0 = TIMER0_RELOAD_LO;
	// уменьшаем счётчик, если он не равен нулю.
	if (ms_ticks_byte) ms_ticks_byte--;
}
