/*
 * main.c
 *
 *  Created on: 09.05.2015
 *      Author: Venci
 */

#include <avr/io.h>
#include <util/delay.h>

#include "DHT.h"

#include "common.h"
#include "timer0.h"
#include "nokia3310.h"
#include "util.h"

int main() {
    uint8_t data[5];

    // CPU PRESCALER
    // enable CPU prescaler changing
    CLKPR = _BV(CLKPCE);

    // set CPU prescaler to 1
    CLKPR = 0;

    // LED
    DDRD |= _BV(DDD6);
    PORTD |= _BV(PD6);

    // LCD
    // инициализируем таймер 0 (для функции delay_ms())
    timer0_init();
    // разрешаем прерывания
    sei();
    // инициализируем дисплей
    nokia_init();
    nokia_cls();
    nokia_gotoxy(0, 0);
    nokia_puts("ThermoLCD v0.1");
    nokia_gotoxy(0, 1);
    nokia_puts_bg("W. Atanasow");
    nokia_gotoxy(0, 2);
    nokia_puts_bg("28.03.2016 g.");
    nokia_gotoxy(0, 4);
    nokia_puts_bg("Zarevdane ...");

    // Sensor
    initDHT();

    // LOOP
    while (1) {
        uint8_t result = readDHT(data);

        // Output
        nokia_cls();

        nokia_gotoxy(0, 0);
        nokia_puts_bg("Temperatura");

        nokia_gotoxy(0, 1);
        nokia_puts_doubled(small_uitoa(data[2]));
        nokia_putchar_doubled(0xc0);
        nokia_putchar_doubled('C');

        nokia_gotoxy(0, 3);
        nokia_puts_bg("Wlavnost");

        nokia_gotoxy(0, 4);
        nokia_puts_doubled(small_uitoa(data[0]));
        nokia_putchar_doubled('%');

        PORTD &= ~_BV(PD6);

        while (result--) {
            PORTB |= _BV(PB7);
            _delay_ms(500);
            PORTB &= ~_BV(PB7);
            _delay_ms(500);
        }

        _delay_ms(1000);

        PORTD |= _BV(PD6);
    }

    return 0;
}
