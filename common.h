#ifndef COMMON_H_INCLUDED
#define COMMON_H_INCLUDED
#include <avr/io.h>
#include <avr/interrupt.h>
#include "macros.h"

typedef unsigned char	u08;
typedef unsigned int	u16;
typedef unsigned long	u32;


#define	FALSE			(0)
#define	TRUE			(!FALSE)

#endif	//	COMMON_H_INCLUDED
