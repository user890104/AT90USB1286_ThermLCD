#ifndef _DHT_h
#define _DHT_h

#define DHT_PORT_OUT PORTA
#define DHT_PORT_IN PINA
#define DHT_PIN_DIR DDRA
#define DHT_PIN PA4

#define SET_BIT(byte, bit) ((byte) |= _BV(bit))

#define CLEAR_BIT(byte, bit) ((byte) &= ~_BV(bit))

#define IS_SET(byte, bit) ((byte) & _BV(bit))

#include <stdint.h>

void initDHT(void);

uint8_t readDHT(uint8_t* data);

#endif
