#include "DHT.h"

#include <avr/io.h>
#include <util/delay.h>

void initDHT(void)
{
    /* According to the DHT11's datasheet, the sensor needs about
       1-2 seconds to get ready when first getting power, so we
       wait
     */
    _delay_ms(2000);
}

uint8_t readDHT(uint8_t* data)
{
    uint8_t cnt, check;
    int8_t i, j;

    /******************* Sensor communication start *******************/

    /* Set data pin as output first */
    SET_BIT(DHT_PIN_DIR, DHT_PIN);

    /* First we need milliseconds delay, so set clk/1024 prescaler */
    TCCR0B = _BV(CS02) | _BV(CS00);

    /* Clear bit for 20 ms */
    CLEAR_BIT(DHT_PORT_OUT, DHT_PIN);

    /* Wait about 20 ms */
    TCNT0 = 0;
    while(TCNT0 < 0x9B); // 10ms

    TCNT0 = 0;
    while(TCNT0 < 0x9B); // 10ms

    /* Now set Timer0 with clk/8 prescaling.
     Gives .5µs per cycle @16Mhz */
    TCCR0B = _BV(CS01);

    TCNT0 = 0;

    /* Pull high again */
    SET_BIT(DHT_PORT_OUT, DHT_PIN);

    /* And set data pin as input */
    CLEAR_BIT(DHT_PIN_DIR, DHT_PIN);

    /* Wait for response from sensor, 20-40µs according to datasheet */
    while (IS_SET(DHT_PORT_IN, DHT_PIN))
    {
        if (TCNT0 >= 0x77) { // 60µs
            return 1;
        }
    }

    /************************* Sensor preamble *************************/

    TCNT0 = 0;

    /* Now wait for the first response to finish, low ~80µs */
    while (!IS_SET(DHT_PORT_IN, DHT_PIN))
    {
        if (TCNT0 >= 0xC7) { // 100µs
            return 2;
        }
    }

    TCNT0 = 0;

    /* Then wait for the second response to finish, high ~80µs */
    while (IS_SET(DHT_PORT_IN, DHT_PIN))
    {
        if (TCNT0 >= 0xC7) { // 100µs
            return 3;
        }
    }

    /********************* Data transmission start **********************/

    for (i = 0; i < 5; ++i)
    {
        for (j = 7; j >= 0; --j)
        {
            TCNT0 = 0;

            /* First there is always a 50µs low period */
            while (!IS_SET(DHT_PORT_IN, DHT_PIN))
            {
                if (TCNT0 >= 0x8B) { // 70µs
                    return 4;
                }
            }

            TCNT0 = 0;

            /* Then the data signal is sent. 26 to 28µs (ideally)
             indicate a low bit, and around 70µs a high bit */
            while (IS_SET(DHT_PORT_IN, DHT_PIN))
            {
                if (TCNT0 >= 0xB3) { // 90µs
                    return 5;
                }
            }

            /* Store the value now so that the whole checking doesn't
             move the TCNT0 forward by too much to make the data look
             bad */
            cnt = TCNT0;

            if (cnt >= 0x27 && cnt <= 0x45) // between 20 and 35 µs
            {
                CLEAR_BIT(data[i], j);
            }
            else
            {
                if (cnt >= 60 && cnt <= 0x9F) // between 0x77 and 80 µs
                {
                    SET_BIT(data[i], j);
                }
                else {
                    return 6;
                }
            }
        }
    }

    /********************* Sensor communication end *********************/

    check = (data[0] + data[1] + data[2] + data[3]) & 0xFF;

    if (check != data[4]) {
        return 7;
    }

    return 0;
}
