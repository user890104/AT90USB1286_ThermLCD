#ifndef UTIL_H_INCLUDED
#define UTIL_H_INCLUDED
#include "common.h"

extern unsigned long small_atoh(u08 * s);
extern u08 small_isxdigit (u08 c);
extern u08 * small_uitoa(u16 value);
extern u08 * small_itoa(int value);

#endif


